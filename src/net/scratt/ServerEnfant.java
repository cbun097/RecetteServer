package net.scratt;

import com.google.gson.GsonBuilder;
import java.net.Socket;
import java.util.ArrayList;

/**
 *
 * @author Claire, Anthony, Genevieve
 */
public class ServerEnfant extends SocketUtil implements Runnable
{
    public ServerEnfant(Socket socket)
    {
        super(socket);
    }

    @Override
    public void run()
    {
        String line;
        // Obtenir la demande du client
        while((line = recevoir()) != null)
        {
            // Splits de la demande
            String[] splits = line.split("::");
            // La commande
            String demande = splits[0];

            //nomcommande::params
            switch(demande)
            {
                // Vérifier les données de connexion
                case "connect" :
                    String username = splits[1].split(";")[0].split("=")[1];
                    String password = splits[1].split(";")[1].split("=")[1];
                    if(Database.getInstance().checkUserCredentials(username, password))
                    {
                        String[] data = Database.getInstance().getUserInfo(username);
                        envoyer(String.format("connect::true::user=%s;pays=%s;email=%s;image=%s;points=%s::%s::%s::%s",data[0],data[1],data[2],data[3],data[4],
                                new GsonBuilder().create().toJson(Database.getInstance().getListeAmies(username)),
                                new GsonBuilder().create().toJson(Database.getInstance().getListeEpicerie(username)),
                                new GsonBuilder().create().toJson(Database.getInstance().getRecetteUser(username))));
                    }
                    else
                    {
                        envoyer("connect::false");
                    }
                    break;
                // Inscrire l'utilisateur dans la BD
                case "inscription" :
                    String user = splits[1].split(";")[0].split("=")[1];
                    String pass = splits[1].split(";")[1].split("=")[1];
                    String email = splits[1].split(";")[2].split("=")[1];
                    String pays = splits[1].split(";")[3].split("=")[1];
                    String urlImage = splits[1].split(";")[4].split("=")[1];
                    int points = Integer.valueOf(splits[1].split(";")[5].split("=")[1]);
                    if(Database.getInstance().insertUsers(user, pass, email, pays, urlImage, points))
                        envoyer("inscription::true");
                    else
                        envoyer("inscription::false");
                    break;
                // Envoyer la liste des recettes
                case "allRecettes" :
                    ArrayList<Recettes> recettes = Database.getInstance().getRecettes();
                    if(recettes != null)
                        envoyer(String.format("allRecettes::%s",new GsonBuilder().create().toJson(recettes)));
                    
                    break;
                // Insérer la recette au calendrier
                case "envoieCalendrier":
                    String userCalendrier = splits[1].split(";")[0].split("=")[1];
                    String nomRecette = splits[1].split(";")[1].split("=")[1];
                    String dateChoisie = splits[1].split(";")[2].split("=")[1];
                    Database.getInstance().insertRecetteCalendrier(nomRecette, userCalendrier, dateChoisie);
                    System.out.println("Insert succes " + userCalendrier + " " + nomRecette + " " + dateChoisie);
                    break;
                // Obtenir la recette du calendrier
                case "askCalendrier":
                    String userAsk = splits[1].split(";")[0].split("=")[1];
                    String date = splits[1].split(";")[1].split("=")[1];
                    envoyer("askCalendrier::" + new GsonBuilder().create().toJson(Database.getInstance().getCalendar(userAsk, date)));
                    System.out.println("askCalendar: " + userAsk + " " + date);
                    break;
                // Ajouter un ami
                case "addAmies":
                    String userID = splits[1].split(";")[0].split("=")[1];
                    String friendID = splits[1].split(";")[1].split("=")[1];
                    if(Database.getInstance().insertAmies(userID, friendID))
                        envoyer("addAmies::true::" + new GsonBuilder().create().toJson(Database.getInstance().getListeAmies(userID)));
                    else
                        envoyer("addAmies::false");
                    break;
                // Ajouter les ingrédients dans la liste d'épicerie
                case "addIngredient" : 
                    String userIDEpicerie = splits[1].split(";")[0].split("=")[1];
                    String[] ingredient = splits[1].split(";")[1].split("=")[1].split("\\$\\$");
                    if(Database.getInstance().saveListeEpicerie(userIDEpicerie, ingredient))
                        envoyer("addIngredient::true::"+new GsonBuilder().create().toJson(Database.getInstance().getListeEpicerie(userIDEpicerie)));
                    else
                        envoyer("addIngredient::false");
                    break;
                // Enlever les ingrédients de la liste d'épicerie
                case "removeIngredient":
                    String removeId = splits[1].split(";")[0].split("=")[1];
                    String[] removeIngredients = splits[1].split(";")[1].split("=")[1].split("\\$\\$");
                    if(Database.getInstance().removeEpicerie(removeId, removeIngredients))
                        envoyer("removeIngredient::true::"+new GsonBuilder().create().toJson(Database.getInstance().getListeEpicerie(removeId)));
                    else
                        envoyer("removeIngredient::false");
                    break;
                // Ajouter un recettes dans les favoris de l'utilisateur
                case "addRecetteUser":
                    String userAddRecette = splits[1].split(";")[0].split("=")[1];
                    String recetteNom = splits[1].split(";")[1].split("=")[1];
                    if(Database.getInstance().insertRecetteUser(userAddRecette, recetteNom))
                        envoyer("addRecetteUser::true::"+new GsonBuilder().create().toJson(Database.getInstance().getRecetteUser(userAddRecette)));
                    else
                        envoyer("addRecetteUser::false");
                    break;
            }
        }
    }
}
