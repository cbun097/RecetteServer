package net.scratt;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Claire, Anthony, Genevieve
 */
public class Database
{
    //propriete url
    private Connection cn;
    //etablir une connexion vers la base de donnees
    private String url;

    //creer la DB
    //Constructeur avec la chaine de connexion
    private Database(String url)
    {
        // Permettre la connection
        this.url = url;
        try
        {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        }
        catch (ClassNotFoundException ex)
        {
            System.err.println(ex.getMessage());
        }
    }

    private static Database instance;
    public static Database getInstance()
    {
        if(instance == null)
            instance = new Database("jdbc:sqlserver://localhost;databaseName=CookingJar;username=sa;password=sql");

        return instance;
    }

    //Verifier si l'utilisateur qui entre dans le login existe ou pas
    public boolean checkUserCredentials(String username, String password)
    {
        try
        {
            cn = DriverManager.getConnection(url);
            PreparedStatement statement = cn.prepareStatement("SELECT * FROM Utilisateurs WHERE Identifiant = ? AND Mdp = ?");
            statement.setString(1, username);
            statement.setString(2, password);
            ResultSet set = statement.executeQuery();
            return set.next();
        }
        catch (SQLException ex)
        {
            System.err.println(ex.getMessage());
        }
        finally
        {
            try
            {
                if(cn != null)
                    cn.close();
            }
            catch (SQLException ex)
            {
                System.err.println(ex.getMessage());
            }
        }

        return false;
    }
    
    //Verifier si l'utilisateur qui entre dans le login existe ou pas
    public boolean insertAmies(String user, String ami)
    {
        try
        {
            cn = DriverManager.getConnection(url);
            PreparedStatement statement = cn.prepareStatement("SELECT * FROM Utilisateurs WHERE Identifiant = ?");
            statement.setString(1, ami);
            ResultSet set = statement.executeQuery();
            if(set.next())
            {
                statement = cn.prepareStatement("INSERT INTO Amies(UserID, FriendID, PointageAmies) VALUES(?,?,?)");
                statement.setString(1, user);
                statement.setString(2, ami);
                statement.setInt(3, set.getInt("Pointage"));
                return statement.executeUpdate() > 0;
            }
        }
        catch (SQLException ex)
        {
            System.err.println(ex.getMessage());
        }
        finally
        {
            try
            {
                if(cn != null)
                    cn.close();
            }
            catch (SQLException ex)
            {
                System.err.println(ex.getMessage());
            }
        }

        return false;
    }
    
    // Obtenir les infos d'un utilisateur
    public String[] getUserInfo(String username)
    {
        try
        {
            String[] data = new String[5];
            cn = DriverManager.getConnection(url);
            PreparedStatement statement = cn.prepareStatement("SELECT * FROM Utilisateurs WHERE Identifiant = ?");
            statement.setString(1, username);
            ResultSet set = statement.executeQuery();
            while(set.next())
            {
                data[0] = set.getString("Identifiant");
                data[1] = set.getString("Pays");
                data[2] = set.getString("Courriel");
                data[3] = set.getString("UrlImage");
                data[4] = String.valueOf(set.getInt("Pointage"));
            }
            return data;
        }
        catch (SQLException ex)
        {
            System.err.println(ex.getMessage());
        }
        finally
        {
            try
            {
                if(cn != null)
                    cn.close();
            }
            catch (SQLException ex)
            {
                System.err.println(ex.getMessage());
            }
        }

        return null;
    }
    
    // Obtenir la liste d'épicerie d'un utilisateur
    public ArrayList<String> getListeEpicerie(String username)
    {
        try
        {
            ArrayList<String> data = new ArrayList<>();
            cn = DriverManager.getConnection(url);
            PreparedStatement statement = cn.prepareStatement("SELECT * FROM ListeEpicerie WHERE UserID = ?");
            statement.setString(1, username);
            ResultSet set = statement.executeQuery();
            while(set.next())
            {
                data.add(set.getString("Ingredient"));
            }
            return data;
        }
        catch (SQLException ex)
        {
            System.err.println(ex.getMessage());
        }
        finally
        {
            try
            {
                if(cn != null)
                    cn.close();
            }
            catch (SQLException ex)
            {
                System.err.println(ex.getMessage());
            }
        }

        return null;
    }
    
    // Enregistrer les ingrédients dans la liste d'épicerie d'un utilisateur
    public boolean saveListeEpicerie(String user, String[] ingredients)
    {
        try
        {
            boolean isDone = false;
            for(String ingredient : ingredients)
            {
                System.out.println(ingredient);
                cn = DriverManager.getConnection(url);
                PreparedStatement statement = cn.prepareStatement("INSERT INTO ListeEpicerie(UserID, Ingredient) VALUES(?,?)");
                statement.setString(1, user);
                statement.setString(2, ingredient);
                isDone = statement.executeUpdate() > 0;
            }
            return isDone;
        }
        catch (SQLException ex)
        {
            System.err.println(ex.getMessage());
        }
        finally
        {
            try
            {
                if(cn != null)
                    cn.close();
            }
            catch (SQLException ex)
            {
                System.err.println(ex.getMessage());
            }
        }

        return false;
    }

    // Obtenir la liste complète des amis d'un utilisateur
    public ArrayList<Users> getListeAmies(String username)
    {
        try
        {
            ArrayList<Users> data = new ArrayList<>();
            cn = DriverManager.getConnection(url);
            PreparedStatement statement = cn.prepareStatement("SELECT * FROM Amies WHERE UserID = ?");
            statement.setString(1, username);
            ResultSet set = statement.executeQuery();
            while(set.next())
            {
                data.add(new Users(set.getString("FriendID"), "not_useful", "not_useful", "not_useful", set.getInt("PointageAmies")));
            }
            return data;
        }
        catch (SQLException ex)
        {
            System.err.println(ex.getMessage());
        }
        finally
        {
            try
            {
                if(cn != null)
                    cn.close();
            }
            catch (SQLException ex)
            {
                System.err.println(ex.getMessage());
            }
        }

        return null;
    }

    // Insérer un utilisateur dans la BD
    public boolean insertUsers(String user, String pass, String email, String pays, String urlImage, int pointage)
    {
        boolean result;
        try
        {
            if(!checkUserCredentials(user, pass))
            {
                cn = DriverManager.getConnection(url);
                //Query pour insert les nouveaux utilisateurs dans la DB
                String query = "INSERT INTO Utilisateurs (Identifiant, Mdp, Pays, Courriel, UrlImage, Pointage) VALUES (?,?,?,?,?,?)";
                PreparedStatement requete = cn.prepareStatement(query);
                requete.setString(1, user);
                requete.setString(2, pass);
                requete.setString(3, email);
                requete.setString(4, pays);
                requete.setString(5, urlImage);
                requete.setInt(6, pointage);
                //executer la requete
                result = requete.executeUpdate() > 0;
                System.out.println("result");
            }
            else
            {
                result = false;
            }
        }
        catch (SQLException ex)
        {
            System.err.println(ex.getMessage());
            result = false;
        }
        finally
        {
            try
            {
                //fermer la connexion
                if(cn != null)
                    cn.close();
            }
            catch (SQLException ex)
            {
                System.err.println(ex.getMessage());
            }
        }
        return result;
    }
    
    // Obtenir toutes les recettes de la BD
    public ArrayList<Recettes> getRecettes()
    {
        ArrayList<Recettes> recettes = new ArrayList<>();
        try
        {
            cn = DriverManager.getConnection(url);
            PreparedStatement statement = cn.prepareStatement("SELECT * FROM Recettes");
            ResultSet set = statement.executeQuery();
            while(set.next())
            {
                recettes.add(new Recettes(set.getString("NomRecette"), set.getString("PaysRecette"), set.getString("DureePrep"),
                                          set.getString("DureeCuisson"), set.getString("tempsAttente"), set.getString("Ingredients"),
                                          set.getString("TypePlat"), set.getString("Preparation"), set.getString("DateAjout"),set.getString("URLImage"),
                                          set.getInt("NiveauRecette"), set.getInt("Calories")));
            }
        }
        catch (SQLException ex)
        {
            System.err.println(ex.getMessage());
        }
        finally
        {
            try
            {
                if(cn != null)
                    cn.close();
            }
            catch (SQLException ex)
            {
                System.err.println(ex.getMessage());
            }
        }

        return recettes;
    }
    
    //insert la recette demandee selon la date de l'utilisateur 
    public boolean insertRecetteCalendrier(String nomRecette, String Identifiant,String date ){
        try
        {
            cn = DriverManager.getConnection(url);
            String query = " INSERT INTO Calendrier(NomRecette, Identifiant, Date) VALUES (?,?,?)";
            PreparedStatement requete = cn.prepareStatement(query);
            requete.setString(1, nomRecette);
            requete.setString(2, Identifiant);
            requete.setString(3, date);
            //executer la requete
            return requete.executeUpdate() > 0;
        } 
        catch (SQLException ex) 
        {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            try
            {
                if(cn != null)
                    cn.close();
            }
            catch (SQLException ex)
            {
                System.err.println(ex.getMessage());
            }
        }
        return false; 
    }
    
    //prendre toutes les recettes inserees a la date selectionnee par utilisateur
    public ArrayList<String> getCalendar(String username, String date)
    {
        try
        {
            ArrayList<String> temp = new ArrayList<>();
            cn = DriverManager.getConnection(url);
            String query = "SELECT NomRecette FROM Calendrier WHERE Identifiant = ? AND Date = ? ";
            PreparedStatement requete = cn.prepareStatement(query);
            requete.setString(1, username);
            requete.setString(2, date);
            ResultSet set = requete.executeQuery();
            while(set.next())
            {
                temp.add(set.getString("NomRecette"));
            }
            return temp;
        } 
        catch (SQLException ex) 
        {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    //remove l'ingredient de la liste d'epicerie de la DB
    public boolean removeEpicerie(String username, String[] ingredients)
    {
        try
        {
            boolean res = false;
            for(String ingredient : ingredients)
            {
                cn = DriverManager.getConnection(url);
                String query = "DELETE FROM ListeEpicerie WHERE UserID=? AND Ingredient LIKE ? ";
                PreparedStatement requete = cn.prepareStatement(query);
                requete.setString(1, username);
                requete.setString(2, ingredient);
                //executer la requete
                res = requete.executeUpdate() > 0;
            }
            return res;
        } 
        catch (SQLException ex)
        {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
         finally
        {
            try
            {
                if(cn != null)
                    cn.close();
            }
            catch (SQLException ex)
            {
                System.err.println(ex.getMessage());
            }
        }
        return false; 
    }
    
    // Inserer une recettes dans la liste des recettes de l'utilisateur
    public boolean insertRecetteUser(String Identifiant, String nomRecette)
    {
        try
        {
            cn = DriverManager.getConnection(url);
            String query = "INSERT INTO RecettesUtilisateurs(Identifiant, NomRecette) VALUES (?,?)";
            PreparedStatement requete = cn.prepareStatement(query);
            requete.setString(1, Identifiant);
            requete.setString(2, nomRecette);
            //executer la requete
            return requete.executeUpdate() > 0;
        } 
        catch (SQLException ex) 
        {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            try
            {
                if(cn != null)
                    cn.close();
            }
            catch (SQLException ex)
            {
                System.err.println(ex.getMessage());
            }
        }
        return false; 
    }

    // Obtenir la liste des recettes d'un utilisateur
    public ArrayList<Recettes> getRecetteUser(String username) 
    {
        ArrayList<Recettes> recettes = new ArrayList<>();
        try
        {
            cn = DriverManager.getConnection(url);
            PreparedStatement statement = cn.prepareStatement("SELECT Recettes.* FROM RecettesUtilisateurs INNER JOIN Recettes ON Recettes.NomRecette = RecettesUtilisateurs.NomRecette WHERE Identifiant = ?");
            statement.setString(1, username);
            ResultSet set = statement.executeQuery();
            while(set.next())
            {
                recettes.add(new Recettes(set.getString("NomRecette"), set.getString("PaysRecette"), set.getString("DureePrep"),
                                          set.getString("DureeCuisson"), set.getString("tempsAttente"), set.getString("Ingredients"),
                                          set.getString("TypePlat"), set.getString("Preparation"), set.getString("DateAjout"),set.getString("URLImage"),
                                          set.getInt("NiveauRecette"), set.getInt("Calories")));
            }
        }
        catch (SQLException ex)
        {
            System.err.println(ex.getMessage());
        }
        finally
        {
            try
            {
                if(cn != null)
                    cn.close();
            }
            catch (SQLException ex)
            {
                System.err.println(ex.getMessage());
            }
        }

        return recettes;
    }
}
