package net.scratt;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.*;

/**
 *
 * @author Claire, Anthony, Genevieve
 */
public class Server extends Thread
{
    private String ip;
    private int port;

    public Server(String ip, int port)
    {
        this.ip = ip;
        this.port = port;
    }

    @Override
    public void run()
    {
        // Création du ThreadPoolExecutor
        ThreadFactory threadFactory = Executors.defaultThreadFactory();
        ThreadPoolExecutor executorPool = new ThreadPoolExecutor(Integer.MAX_VALUE, Integer.MAX_VALUE, 10, TimeUnit.SECONDS, new ArrayBlockingQueue(2), threadFactory,
                (r, executor) -> {
                    throw new UnsupportedOperationException("Not supported yet.");
                }
        );
        try
        {
            // Démarrer le serveur
            ServerSocket socket = new ServerSocket();
            socket.bind(new InetSocketAddress(ip, port));
            while(true)
            {
                System.out.println("Waiting connection...");
                try
                {
                    // Obtenir la connexion
                    Socket sCom = socket.accept();
                    System.out.println("Send connection to communication thread...");
                    // Démarrer la communication
                    executorPool.execute(new ServerEnfant(sCom));
                }
                catch(IOException ex)
                {
                    ex.printStackTrace();
                }
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
