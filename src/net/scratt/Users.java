package net.scratt;

/**
 *
 * @author Claire, Anthony, Genevieve
 */
public class Users
{
    private String username, pays, courriel, urlImage;
    private int points;

    public Users(String username, String pays, String courriel, String urlImage, int points)
    {
        this.username = username;
        this.pays = pays;
        this.courriel = courriel;
        this.urlImage = urlImage;
        this.points = points;
    }
    
    public String getUsername() 
    {
        return username;
    }

    public void setUsername(String username) 
    {
        this.username = username;
    }

    public String getPays() 
    {
        return pays;
    }

    public void setPays(String pays) 
    {
        this.pays = pays;
    }

    public String getCourriel() 
    {
        return courriel;
    }

    public void setCourriel(String courriel) 
    {
        this.courriel = courriel;
    }

    public String getUrlImage() 
    {
        return urlImage;
    }

    public void setUrlImage(String urlImage)
    {
        this.urlImage = urlImage;
    }

    public int getPoints() 
    {
        return points;
    }

    public void setPoints(int points) 
    {
        this.points = points;
    }
}
